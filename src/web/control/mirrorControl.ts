import {
  GENERIC_CLOSE,
  MIRROR_DESC_0,
  MIRROR_DESC_0_NO_TOKEN,
  MIRROR_DESC_1,
  MIRROR_HINT_0,
  MIRROR_HINT_1,
  MIRROR_HINT_CLICK_TO_CHANGE,
  MIRROR_LANDING_CONFLICT_DESC,
  MIRROR_LANDING_CONFLICT_KEEP,
  MIRROR_LANDING_CONFLICT_OVERWRITE,
  MIRROR_LANDING_CONFLICT_TITLE,
  MIRROR_LANDING_INVALID_REFERRAL,
  MIRROR_LANDING_SUCCESS_HINT,
  MIRROR_PROVIDED_BY,
  MIRROR_TECHNOLOGY,
  MIRROR_TITLE,
  MIRROR_URL,
} from '../constant/messages';
import { mirrorSites, mirrorSitesPlusMainSite } from '../constant/mirrorSites';
import { mirrorLandingHref } from '../data/hrefs';
import { h } from '../hs';
import { padName } from '../util/padName';
import { PathHandler } from './followQuery';
import { createHint } from './hintControl';
import { confirm, Modal, showGenericError } from './modalControl';
import { tokenItem } from './userControl';

export function showMirrorSitesModal(scroll?: number) {
  const modal = new Modal(h('.mirror-site-modal', [
    h('h1', MIRROR_TITLE),
    h('p', tokenItem.exists() ? MIRROR_DESC_0 : MIRROR_DESC_0_NO_TOKEN),
    h('p', MIRROR_DESC_1),
    h('.button-container', [
      ...mirrorSitesPlusMainSite.map(({ name, origin, provider, technology }) => {
        const $button = h('a.rich', {
          href: (origin === window.location.origin) ? '#' : mirrorLandingHref(origin, tokenItem.getValue()),
          onclick: (event: MouseEvent) => {
            event.preventDefault();
            if (origin === window.location.origin) {
              return;
            }
            window.location.replace(mirrorLandingHref(origin, tokenItem.getValue(), modal.modal.scrollTop));
          },
        }, [
          h('h2', name),
          h('p', [
            MIRROR_URL + origin,
            h('br'),
            MIRROR_PROVIDED_BY + provider,
            h('br'),
            MIRROR_TECHNOLOGY + technology,
          ]),
        ]);
        if (origin === window.location.origin) {
          $button.classList.add('selected');
        }
        return $button;
      }),
      h('div', {
        onclick: () => modal.close(),
      }, GENERIC_CLOSE),
    ]),
  ]) as HTMLDivElement);
  modal.setDismissible();
  modal.open();
  if (scroll !== undefined) {
    modal.modal.scrollTop = scroll;
  }
}

export const mirrorLandingHandler: PathHandler = (_, args) => {
  // Prevent leaving the token in browser history
  window.history.replaceState(null, document.title, '#/mirror-landing');
  window.location.hash = '#';
  showMirrorSitesModal(args.has('scroll') ? +args.get('scroll')! : undefined);
  const newToken = args.get('token');
  if (newToken === undefined) {
    return true;
  }
  if (!mirrorSitesPlusMainSite.some(
    mirror => document.referrer === mirror.origin
      || document.referrer.startsWith(mirror.origin + '/'))
  ) {
    showGenericError(MIRROR_LANDING_INVALID_REFERRAL);
    return true;
  }
  const oldToken = tokenItem.getValue();
  if (oldToken !== null) {
    if (oldToken !== newToken) {
      confirm(
        MIRROR_LANDING_CONFLICT_TITLE,
        MIRROR_LANDING_CONFLICT_DESC,
        MIRROR_LANDING_CONFLICT_OVERWRITE,
        MIRROR_LANDING_CONFLICT_KEEP,
      ).then(result => {
        if (result) {
          // Overwrite
          tokenItem.setValue(newToken);
          createHint(MIRROR_LANDING_SUCCESS_HINT);
        }
      });
    }
  } else {
    // No conflict
    tokenItem.setValue(newToken);
    createHint(MIRROR_LANDING_SUCCESS_HINT);
  }

  return true;
};

const currentMirrorSite = mirrorSites.find(({ origin }) => origin === window.location.origin);
if (currentMirrorSite !== undefined) {
  const $mirrorHint = h('.mirror-hint', [
    MIRROR_HINT_0.replace('$', padName(currentMirrorSite.provider)),
    h('span', {
      onclick: () => showMirrorSitesModal(),
    }, MIRROR_HINT_CLICK_TO_CHANGE),
    MIRROR_HINT_1,
  ]);
  document.body.append($mirrorHint);
}
